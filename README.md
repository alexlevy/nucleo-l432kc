# Nucleo STM32L432KC HAL

Declarative HAL for Nucleo STM32L432KC microcontroller using Rust macros.

## Applications

### Temperature and humidity acquisition and display

* DHT11 sensor that captures temperature and humidity data
* Display on either [LCD 1602](https://datasheet4u.com/datasheet-pdf/CA/LCD-1602A/pdf.php?id=519148) or [OLED SSD1306](https://www.alldatasheet.com/datasheet-pdf/pdf/1179026/ETC2/SSD1306.html) (chosen at compile time through features)

## Run

### Debug

* `cargo rb dht11 --features oled` or
* `cargo rb dht11 --features lcd`
### Release

Use `rrb` instead of `rb`

## Debug

### Windows

* Re-compile openocd in UCRT64 [msys2](https://www.msys2.org/) shell
```shell
$ pacman -Syu
$ pacman -S make libtool pkg-config autoconf automake texinfo ucrt64/mingw-w64-ucrt-x86_64-gcc mingw-w64-ucrt-x86_64-toolchain
$ ./configure --enable-stlink --disable-ftdi --disable-ti-icdi --disable-ulink --disable-usb-blaster-2 --disable-ft232r --disable-vslink --disable-xds110 --disable-cmsis-dap-v2 --disable-osbdm --disable-opendous --disable-armjtagew --disable-rlink --disable-usbprog --disable-esp-usb-jtag --disable-cmsis-dap --disable-nulink --disable-kitprog --disable-usb-blaster --disable-presto --disable-openjtag --disable-buspirate --disable-jlink --disable-aice --disable-parport --disable-parport-ppdev --disable-parport-giveio --disable-jtag_vpi --disable-vdebug --disable-jtag_dpi --disable-amtjtagaccel --disable-bcm2835gpio --disable-imx_gpio --disable-am335xgpio --disable-ep93xx --disable-at91rm9200 --disable-gw16012 --disable-sysfsgpio --disable-xlnx-pcie-xvc --disable-remote-bitbang --enable-verbose-usb-io --enable-verbose-usb-comms
$ make && make install
```
* Install [ST-Link driver](https://www.st.com/en/development-tools/stsw-link009.html)
* Install [ARM embedded toolchain](https://developer.arm.com/downloads/-/gnu-rm)
* In project folder, run `openocd -f openocd.cfg`
* Run ARM `GCC` command prompt and in project folder run `arm-none-eabi-gdb -x openocd.gdb -f target\thumbv7em-none-eabihf\debug\dht11`
