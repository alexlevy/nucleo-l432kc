#![no_main]
#![no_std]
#![allow(unused_imports)]
#![allow(dead_code)]

use basic_timers::{Startable, Timer};
use core::marker::PhantomData;
use core::ptr;
use core::{cell::RefCell, ops::DerefMut, result::Result};
use cortex_m::interrupt::Mutex;
use cortex_m_rt::exception;
use heapless::String;
use heapless::Vec;
use numtoa::NumToA;

use stm32l4::stm32l4x2::{
    interrupt, rcc::cr::MSIRANGE_A::*, Peripherals, EXTI, GPIOA, GPIOB, I2C1, RCC, SPI1, SYSCFG,
    TIM6, TIM7,
};

use nucleo_l432kc::{
    clocks::{Clock, PllDivisor},
    flash::Flash,
    gpio,
    gpio::{Floating, Output, PushPull, UnknownMode},
    i2c, lcd_1602,
    power::Power,
    spi, timers,
    timers::TimerConfigured,
};

static mut DHT11: Vec<u16, 42> = Vec::new();

// This depends on the CPU frequency. DHT11 holds high for 26-28us if the bit is a 0. At 18MHz
// frequency 1_440 is 80us = 50us that DHT11 holds low before transmitting datum + 30us. If
// signal is still low after that then DHT11 sent a 1.
const DHT11_MAX_TICKS_BIT_0: u16 = 1_440;

// A8: LED that blinks when DHT11 samples temp and humidity
// A5: SPI SCLK for OLED
// A7: SPI MOSI for OLED
// A4: RST pin of OLED
// A6: D/C pin of OLED
//
// B1: DHT11 pin
// B6: SCL pin for LCD
// B7: SDA pin for LCD
gpio!(
    A { 8 ; 5 => +afrl=af5 ; 7 => +afrl=af5 ; 6 ; 4 }
    B [ EXTI1 ] { 1 =>  exticr1=0b001,imr1,ftsr1=enabled,rtsr1=disabled ; 6 => +afrl=af4 ; 7 => +afrl=af4 }
);

#[cfg(feature = "lcd")]
i2c!(1 => presc=0x3, sdadel=0x2, scldel=0x4, scll=0xe1, sclh=0xdd);

#[cfg(feature = "lcd")]
lcd_1602!(I2C1, B { 6 ; 7 });

timers!(TIM6 [ TIM6_DACUNDER ] ; TIM7);

#[cfg(feature = "oled")]
spi!(1 => { apb2rstr, apb2enr, bidimode=0, bidioe=1, rxonly=0, ssm=0, lsbfirst=0, br=Div16, mstr=1, cpol=1, cpha=1, ssoe=1, ds=8Bits });

static SYSCFG: Mutex<RefCell<Option<SYSCFG>>> = Mutex::new(RefCell::new(None));
static EXTI: Mutex<RefCell<Option<EXTI>>> = Mutex::new(RefCell::new(None));
static TIMER: Mutex<RefCell<Option<Timer<TIM6, TimerConfigured>>>> = Mutex::new(RefCell::new(None));
static TIM6: Mutex<RefCell<Option<TIM6>>> = Mutex::new(RefCell::new(None));

static GPIOA: Mutex<RefCell<Option<GPIOA>>> = Mutex::new(RefCell::new(None));
static PA8: Mutex<RefCell<Option<PA8<Output<PushPull, PullDown>>>>> =
    Mutex::new(RefCell::new(None));

static GPIOB: Mutex<RefCell<Option<GPIOB>>> = Mutex::new(RefCell::new(None));
static PB1: Mutex<RefCell<Option<PB1<UnknownMode>>>> = Mutex::new(RefCell::new(None));

#[cfg(feature = "lcd")]
static LCD: Mutex<RefCell<Option<LCD<LCDInitialized>>>> = Mutex::new(RefCell::new(None));
#[cfg(feature = "lcd")]
static I2C1: Mutex<RefCell<Option<stm32l4::stm32l4x2::I2C1>>> = Mutex::new(RefCell::new(None));
#[cfg(feature = "lcd")]
static I2C: Mutex<RefCell<Option<I2C<stm32l4::stm32l4x2::I2C1, I2CEnabled>>>> =
    Mutex::new(RefCell::new(None));

#[cfg(feature = "oled")]
static SPI1: Mutex<RefCell<Option<stm32l4::stm32l4x2::SPI1>>> = Mutex::new(RefCell::new(None));
#[cfg(feature = "oled")]
static SPI: Mutex<RefCell<Option<SPI<stm32l4::stm32l4x2::SPI1, SPIConfigured>>>> =
    Mutex::new(RefCell::new(None));
#[cfg(feature = "oled")]
static PA6: Mutex<RefCell<Option<PA6<Output<PushPull, PullDown>>>>> =
    Mutex::new(RefCell::new(None));

#[cortex_m_rt::entry]
fn main() -> ! {
    let mut dp = Peripherals::take().unwrap();

    let cp = cortex_m::Peripherals::take().unwrap();

    let rcc = &mut dp.RCC;
    let gpioa = &mut dp.GPIOA;
    let flash = &mut dp.FLASH;
    let pwr = &mut dp.PWR;
    let syscfg = &mut dp.SYSCFG;
    let exti = &mut dp.EXTI;
    let tim6 = &mut dp.TIM6;

    #[cfg(feature = "lcd")]
    let gpiob = &mut dp.GPIOB;

    #[cfg(feature = "lcd")]
    let i2c1 = &mut dp.I2C1;

    #[cfg(feature = "oled")]
    let spi1 = &mut dp.SPI1;

    #[cfg(feature = "oled")]
    let tim7 = &mut dp.TIM7;

    Flash::new(flash).one_wait_state();

    Power::new(rcc, pwr).enable().range1();

    // MSI clock + PLL at 18MHz (8 / 2 * 9 / 2)
    Clock::new(rcc)
        .msi(Range8m)
        .multiply_vco_by(9)
        .divide_vco_by(2)
        .divide_pll_by(PllDivisor::Div2)
        .enable_pll()
        .start_pll();

    let periph = <Peripheral<GPIOA, _>>::enable(rcc);

    // DHT11 pulse LED
    let pa8 = PA8::new(&periph)
        .into_output(gpioa)
        .push_pull(gpioa)
        .pull_down(gpioa);

    #[cfg(feature = "oled")]
    {
        PA5::new(&periph).into_alternate(gpioa);
        PA7::new(&periph).into_alternate(gpioa);
    }

    #[cfg(feature = "oled")]
    let pa6 = PA6::new(&periph)
        .into_output(gpioa)
        .push_pull(gpioa)
        .pull_down(gpioa);

    #[cfg(feature = "oled")]
    let mut pa4 = PA4::new(&periph)
        .into_output(gpioa)
        .push_pull(gpioa)
        .pull_up(gpioa);

    let periph = <Peripheral<GPIOB, _>>::enable(rcc);

    let pb1 = PB1::new(&periph)
        .configure_interrupt(syscfg, exti)
        .disable_interrupt(exti);

    #[cfg(feature = "lcd")]
    let i2c = <I2C<I2C1, _>>::activate(rcc)
        .configure(i2c1)
        .enable_analog_filter(i2c1)
        .configure_timings(i2c1)
        .enable(i2c1);

    #[cfg(feature = "lcd")]
    let lcd = initialize_lcd(rcc, &i2c, i2c1, gpiob);

    #[cfg(feature = "oled")]
    let mut spi = <SPI<SPI1, _>>::activate(rcc).configure(spi1);

    #[cfg(feature = "oled")]
    initialize_oled(&mut spi, spi1, rcc, tim7, &mut pa4, gpioa);

    // Setting update request source to not trigger an interrupt when manually triggering an update
    let timer = <Timer<TIM6, _>>::new(tim6, rcc)
        .one_shot(tim6)
        .set_update_request_source(tim6);

    cortex_m::interrupt::free(|cs| {
        TIMER.borrow(cs).replace(Some(timer));
        TIM6.borrow(cs).replace(Some(dp.TIM6));
        SYSCFG.borrow(cs).replace(Some(dp.SYSCFG));
        EXTI.borrow(cs).replace(Some(dp.EXTI));
        GPIOA.borrow(cs).replace(Some(dp.GPIOA));
        PA8.borrow(cs).replace(Some(pa8));
        GPIOB.borrow(cs).replace(Some(dp.GPIOB));
        PB1.borrow(cs).replace(Some(pb1));

        #[cfg(feature = "lcd")]
        {
            LCD.borrow(cs).replace(Some(lcd));
            I2C1.borrow(cs).replace(Some(dp.I2C1));
            I2C.borrow(cs).replace(Some(i2c));
        }

        #[cfg(feature = "oled")]
        {
            SPI1.borrow(cs).replace(Some(dp.SPI1));
            SPI.borrow(cs).replace(Some(spi));
            PA6.borrow(cs).replace(Some(pa6));
        }
    });

    trigger_dht11_acquisition();

    let mut systick = cp.SYST;
    let ticks = cortex_m::peripheral::SYST::get_ticks_per_10ms() * 540;

    systick.set_reload(ticks);
    systick.clear_current();
    systick.enable_interrupt();
    systick.enable_counter();

    loop {}
}

#[cfg(feature = "oled")]
fn initialize_oled(
    spi: &mut SPI<SPI1, SPIConfigured>,
    spi1: &mut SPI1,
    rcc: &mut RCC,
    tim7: &mut TIM7,
    pa4: &mut PA4<Output<PushPull, PullUp>>,
    gpioa: &mut GPIOA,
) {
    #[allow(non_snake_case)]
    let SPI_DR: *mut u8 = 0x4001_300C as *mut _;

    spi.enable(spi1);

    let timer = <Timer<TIM7, _>>::new(tim7, rcc).one_shot(tim7);

    // RST routine
    pa4.set_low(gpioa);

    // Wait for 4us (min 3us from datasheet)
    timer
        .load(tim7, 72, Some(0))
        .start(tim7)
        .wait_for_overflow(tim7);

    pa4.set_high(gpioa);
    // End RST routine

    // By default PA6 (DC pin) is low so no need to pull it down
    // Charge pump setting
    unsafe { ptr::write_volatile(SPI_DR, 0x8D as u8) };
    // Charge pump enable
    unsafe { ptr::write_volatile(SPI_DR, 0x14 as u8) };
    // Display ON
    unsafe { ptr::write_volatile(SPI_DR, 0xAF as u8) };

    // Wait 100ms for SEG/COM to come online
    timer
        .load(tim7, 1_800, Some(1_000))
        .start(tim7)
        .wait_for_overflow(tim7);

    // Fill display
    unsafe { ptr::write_volatile(SPI_DR, 0xA5 as u8) };
    while spi1.sr.read().bsy().bit_is_set() {}
}

#[cfg(feature = "lcd")]
fn initialize_lcd(
    rcc: &mut RCC,
    i2c: &I2C<I2C1, I2CEnabled>,
    i2c1: &mut I2C1,
    gpiob: &mut GPIOB,
) -> LCD<LCDInitialized> {
    let lcd = LCD::init(rcc, gpiob, 0x27);

    lcd.define_custom_character(
        i2c,
        i2c1,
        0x0,
        &[0x0c, 0x12, 0x12, 0x0c, 0x0, 0x0, 0x0, 0x0], // Celcius degree symbol
    )
    .clear_screen(i2c, i2c1);

    lcd
}

/// Acquire temperature and relative humidity data from DHT11 sensor
///
/// 1. GPIO B1 `Output<OpenDrain, Floating>` is pulled low for 20ms to wake up DHT11
/// 2. GPIO B1 is pulled high by converting GPIO PB1 to `Input<Floating>` (DHT11 has an internal 10K pull up resistor)
/// 3. TIM6 is set as a timeout of 1ms to measure how long subsequent signals are pulled high by DHT11 when it sends data.
/// 4. GPIO PB1 interrupts are enabled on falling edge. Interrupts record the time it took from the previous falling edge to the current one. Data is recorded in a `Vec<u16, U42>`
/// 5. When TIM6 overflows (1ms) we know data transmission is over so we decode the data and calculate the checksum.
///
/// # DHT11 protocol
///
/// | Signal             | Description                                                                                                        |
/// |--------------------|--------------------------------------------------------------------------------------------------------------------|
/// | ↓ 20ms ↑           | MCU pulls signal low for 20ms (min = 18ms from DHT11 datasheet) to wake up the sensor then MCU pulls high          |
/// | ↑ 20-30us ↓        | DHT11 pulls the signal low before sending its response                                                             |
/// | ↓ 80us ↑ 80us ↓    | DHT11 pulls low for 80us then pulls high for 80us as a response to wake up signal                                  |
/// | ↓ 50us ↑ 26-28us ↓ | DHT11 pulls low for 50us to prepare transmission then pulls high for 26-28us which means it sent a bit that is a 0 |
/// | ↓ 50us ↑ 70us ↓    | DHT11 pulls low for 50us to prepare transmission then pulls high for 70us which means it sent a bit that is a 1    |
/// | ↓ ↑ ∞              | DHT11 has finished transmitting data                                                                               |
fn trigger_dht11_acquisition() {
    cortex_m::interrupt::free(|cs| {
        let mtx = TIMER.borrow(cs).borrow();
        let timer = mtx.as_ref().unwrap();

        let mtx = PB1.borrow(cs).borrow();
        let pb1 = mtx.as_ref().unwrap();

        let mut mtx = EXTI.borrow(cs).borrow_mut();
        let exti = mtx.as_mut().unwrap();

        let mut mtx = GPIOB.borrow(cs).borrow_mut();
        let gpiob = mtx.as_mut().unwrap();

        let mut mtx = TIM6.borrow(cs).borrow_mut();
        let tim6 = mtx.as_mut().unwrap();

        pb1.disable_interrupt(exti)
            .into_output(gpiob)
            .open_drain(gpiob)
            .floating(gpiob);

        // Low signal for 20ms. We synchronously wait here effectively blocking the CPU for 20ms
        timer
            .load(tim6, 3_600, Some(100))
            .start(tim6)
            .wait_for_overflow(tim6);

        // Prepare for sensor response
        pb1.into_input(gpiob).pull_up(gpiob).enable_interrupt(exti);

        // Timer is now a 1ms timeout as each response from the sensor is shorter than that
        timer
            .enable_interrupts(tim6)
            .load(tim6, 18_000, Some(1))
            .start(tim6);
    });
}

fn read_dht11_data(dht_data: Vec<u16, 42>) -> (u8, u8, u8, u8, u8) {
    let mut int_rh: u8 = 0;
    let mut dec_rh: u8 = 0;
    let mut int_t: u8 = 0;
    let mut dec_t: u8 = 0;
    let mut csum: u8 = 0;

    let mut i: u8 = 0;

    for x in dht_data.iter().skip(2) {
        let idx = i % 8;
        match i {
            0..=7 => int_rh = read_dht11_bit(int_rh, idx, x),
            8..=15 => dec_rh = read_dht11_bit(dec_rh, idx, x),
            16..=23 => int_t = read_dht11_bit(int_t, idx, x),
            24..=31 => dec_t = read_dht11_bit(dec_t, idx, x),
            32..=39 => csum = read_dht11_bit(csum, idx, x),
            _ => unreachable!(),
        }
        i += 1;
    }

    (int_rh, dec_rh, int_t, dec_t, csum)
}

#[cfg(feature = "lcd")]
fn display_dht11_data_on_lcd(int_rh: u8, dec_rh: u8, int_t: u8, dec_t: u8) {
    let data = format_dht11_data(int_t, dec_t);
    display_buffer_on_lcd(0x0, 0x0, &data, Some(0x0), "C");

    let data = format_dht11_data(int_rh, dec_rh);
    display_buffer_on_lcd(0x1, 0x0, &data, None, "%");
}

fn evaluate_dht11_checksum(
    int_rh: u8,
    dec_rh: u8,
    int_t: u8,
    dec_t: u8,
    csum: u8,
) -> Result<(), ()> {
    cortex_m::interrupt::free(|cs| {
        let mut mtx = GPIOA.borrow(cs).borrow_mut();
        let gpioa = mtx.as_mut().unwrap();

        let mtx = PA8.borrow(cs).borrow();
        let pa8 = mtx.as_ref().unwrap();

        if (int_rh + dec_rh + int_t + dec_t) & 0xFF != csum {
            pa8.set_high(gpioa);
            return Err(());
        }

        pa8.set_low(gpioa);

        Ok(())
    })
}

#[cfg(feature = "display")]
fn format_dht11_data(int_part: u8, dec_part: u8) -> String<6> {
    let mut buf = [0u8; 3];
    let mut data: String<6> = String::new();

    data.push_str(int_part.numtoa_str(10, &mut buf)).unwrap();
    data.push('.').unwrap();
    data.push_str(dec_part.numtoa_str(10, &mut buf)).unwrap();

    data
}

#[cfg(feature = "lcd")]
fn display_buffer_on_lcd(
    row: u8,
    column: u8,
    data: &String<6>,
    custom_char: Option<u8>,
    suffix: &str,
) {
    cortex_m::interrupt::free(|cs| {
        let mtx = LCD.borrow(cs).borrow();
        let lcd = mtx.as_ref().unwrap();

        let mtx = I2C.borrow(cs).borrow();
        let i2c = mtx.as_ref().unwrap();

        if let Some(ref mut i2c1) = I2C1.borrow(cs).borrow_mut().deref_mut() {
            lcd.set_cursor_position(i2c, i2c1, row, column)
                .print_string(i2c, i2c1, data.as_str());

            if let Some(c) = custom_char {
                lcd.print_custom_character(i2c, i2c1, c);
            }

            lcd.print_string(i2c, i2c1, suffix);
        }
    });
}

#[inline(always)]
fn read_dht11_bit(val: u8, offset: u8, ticks: &u16) -> u8 {
    // If number of ticks is greater than DHT11_MAX_TICKS_BIT_0, it means the received bit is 1
    if *ticks > DHT11_MAX_TICKS_BIT_0 {
        val | (1 << (7 - offset))
    } else {
        val
    }
}

#[interrupt]
#[allow(non_snake_case)]
fn EXTI1() {
    cortex_m::interrupt::free(|cs| {
        let mtx = EXTI.borrow(cs).borrow();
        let exti = mtx.as_ref().unwrap();

        // Clear the interrupt flag
        exti.pr1.modify(|_, w| w.pr1().set_bit());

        let mtx = TIMER.borrow(cs).borrow();
        let timer = mtx.as_ref().unwrap();

        let mut mtx = TIM6.borrow(cs).borrow_mut();
        let tim6 = mtx.deref_mut().as_mut().unwrap();

        timer.stop(tim6);

        unsafe { DHT11.push(tim6.cnt.read().cnt().bits()).unwrap() };

        timer.trigger_update(tim6).start(tim6);
    });
}

#[interrupt]
#[allow(non_snake_case)]
fn TIM6_DACUNDER() {
    let dht_data = cortex_m::interrupt::free(|cs| {
        let mtx = TIMER.borrow(cs).borrow();
        let timer = mtx.as_ref().unwrap();

        if let Some(ref mut tim6) = TIM6.borrow(cs).borrow_mut().deref_mut() {
            timer.clear_update_interrupt_flag(tim6);
        }

        if unsafe { DHT11.len() } <= 0 {
            if let Some(ref mut gpioa) = GPIOA.borrow(cs).borrow_mut().deref_mut() {
                let mtx = PA8.borrow(cs).borrow();
                let pa8 = mtx.as_ref().unwrap();
                pa8.set_high(gpioa);
            }
        }

        // We copy the data so that we don't disable interrupts for too long
        unsafe {
            let d = DHT11.clone();
            DHT11.clear();
            d
        }
    });

    // PC will be restored at the end of this routine but at least we allow interrupts to be queued
    if dht_data.len() > 0 {
        let (int_rh, dec_rh, int_t, dec_t, csum) = read_dht11_data(dht_data);

        let csum_res = evaluate_dht11_checksum(int_rh, dec_rh, int_t, dec_t, csum);

        if csum_res.is_ok() {
            #[cfg(feature = "lcd")]
            display_dht11_data_on_lcd(int_rh, dec_rh, int_t, dec_t);
        }
    }
}

#[exception]
#[allow(non_snake_case)]
fn SysTick() {
    trigger_dht11_acquisition();
}
