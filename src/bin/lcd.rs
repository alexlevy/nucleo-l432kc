#![no_main]
#![no_std]

use core::marker::PhantomData;

use nucleo_l432kc::{
    clocks::{Clock, PllDivisor},
    flash::Flash,
    gpio, i2c, lcd_1602,
    power::Power,
};
use stm32l4::stm32l4x2::{rcc::cr::MSIRANGE_A::*, Peripherals, I2C1, I2C2, RCC};

gpio!( B { 6 => +afrl=af4 ; 7 => +afrl=af4 });
i2c!(
    1 => presc=0x3, sdadel=0x2, scldel=0x4, scll=0xe1, sclh=0xdd;
    2 => presc=0x3, sdadel=0x2, scldel=0x4, scll=0x31, sclh=0x12
);
lcd_1602!(I2C1, B { 6 ; 7 });

#[cortex_m_rt::entry]
fn main() -> ! {
    let mut dp = Peripherals::take().unwrap();

    Flash::new(&mut dp.FLASH).one_wait_state();

    Power::new(&mut dp.RCC, &mut dp.PWR).enable().range1();

    // MSI clock + PLL at 18MHz (8 / 2 * 9 / 2)
    Clock::new(&mut dp.RCC)
        .msi(Range8m)
        .multiply_vco_by(9)
        .divide_vco_by(2)
        .divide_pll_by(PllDivisor::Div2)
        .enable_pll()
        .start_pll();

    let i2c = <I2C<I2C1, _>>::activate(&mut dp.RCC)
        .configure(&mut dp.I2C1)
        .enable_analog_filter(&mut dp.I2C1)
        .configure_timings(&mut dp.I2C1)
        .enable(&mut dp.I2C1);

    // Just to test a second I2C device
    let _ = <I2C<I2C2, _>>::activate(&mut dp.RCC);

    let lcd = LCD::init(&mut dp.RCC, &mut dp.GPIOB, 0x27);

    lcd.clear_screen(&i2c, &mut dp.I2C1)
        .print_string(&i2c, &mut dp.I2C1, "Yo")
        .set_cursor_position(&i2c, &mut dp.I2C1, 0x1, 0x0)
        .print_string(&i2c, &mut dp.I2C1, "world!");

    loop {}
}
