use core::marker::PhantomData;

use defmt::assert;

use stm32l4::stm32l4x2::{rcc::cr::MSIRANGE_A, RCC};

// Clock types
pub struct Msi;
pub struct NoClockSource;

// Clock state
pub struct NewClock;
pub struct ClockInitialized;
pub struct ClockStarted;

pub struct PllOn;
pub struct PllOff;
pub struct PllConfigured;
pub struct PllEnabled;
pub struct PllScaled;

pub struct Clock<'a, TYPE, PLL, STATE> {
    rcc: &'a mut RCC,
    _clock_type: PhantomData<TYPE>,
    _state: PhantomData<STATE>,
    _pll: PhantomData<PLL>,
}

pub enum PllDivisor {
    Div2 = 0b00,
    Div4 = 0b01,
    Div6 = 0b10,
    Div8 = 0b11,
}

impl<'a> Clock<'a, NoClockSource, PllOff, NewClock> {
    pub fn new(rcc: &'a mut RCC) -> Self {
        Clock {
            rcc,
            _clock_type: PhantomData,
            _state: PhantomData,
            _pll: PhantomData,
        }
    }

    pub fn msi(self, range: MSIRANGE_A) -> Clock<'a, Msi, PllOff, ClockInitialized> {
        self.rcc.cr.modify(|_, w| {
            w.msirgsel()
                .set_bit()
                .msirange()
                .variant(range)
                .msion()
                .set_bit()
        });

        Clock {
            rcc: self.rcc,
            _clock_type: PhantomData,
            _state: PhantomData,
            _pll: PhantomData,
        }
    }
}

impl<'a, TYPE> Clock<'a, TYPE, PllOff, ClockInitialized> {
    pub fn multiply_vco_by(self, factor: u8) -> Self {
        assert!(
            factor <= 86 && factor >= 8,
            "PLL multiplicator should be between 8 and 86"
        );
        self.rcc
            .pllcfgr
            .modify(|_, w| unsafe { w.plln().bits(factor) });
        self
    }

    pub fn divide_vco_by(self, factor: u8) -> Self {
        assert!(
            factor <= 8 && factor >= 1,
            "PLL divisor should be between 1 and 8"
        );
        self.rcc
            .pllcfgr
            .modify(|_, w| unsafe { w.pllm().bits(factor - 1) });
        self
    }

    pub fn divide_pll_by(
        self,
        factor: PllDivisor,
    ) -> Clock<'a, TYPE, PllConfigured, ClockInitialized> {
        self.rcc
            .pllcfgr
            .modify(|_, w| unsafe { w.pllr().bits(factor as u8) });

        Clock {
            rcc: self.rcc,
            _clock_type: PhantomData,
            _state: PhantomData,
            _pll: PhantomData,
        }
    }
}

impl<'a> Clock<'a, Msi, PllConfigured, ClockInitialized> {
    pub fn enable_pll(self) -> Clock<'a, Msi, PllEnabled, ClockInitialized> {
        // Set MSI as source
        self.rcc
            .pllcfgr
            .modify(|_, w| unsafe { w.pllsrc().bits(0b01) });

        self.rcc.cr.modify(|_, w| w.pllon().set_bit());

        // TODO: setup a timeout and panic if overflow
        while self.rcc.cr.read().pllrdy().bit_is_clear() {}

        self.rcc.pllcfgr.modify(|_, w| w.pllren().set_bit());

        Clock {
            rcc: self.rcc,
            _state: PhantomData,
            _clock_type: PhantomData,
            _pll: PhantomData,
        }
    }
}

impl<'a, TYPE> Clock<'a, TYPE, PllEnabled, ClockInitialized> {
    pub fn start_pll(self) -> Clock<'a, TYPE, PllOn, ClockInitialized> {
        self.rcc.cfgr.modify(|_, w| unsafe { w.sw().bits(0b11) });

        while self.rcc.cfgr.read().sws().bits() != 0b11 {}

        Clock {
            rcc: self.rcc,
            _state: PhantomData,
            _clock_type: PhantomData,
            _pll: PhantomData,
        }
    }
}

impl<'a> Clock<'a, Msi, PllOff, ClockInitialized> {
    pub fn start_msi(self) -> Clock<'a, Msi, PllOff, ClockStarted> {
        self.rcc.cfgr.modify(|_, w| unsafe { w.sw().bits(0b00) });

        Clock {
            rcc: self.rcc,
            _state: PhantomData,
            _clock_type: PhantomData,
            _pll: PhantomData,
        }
    }
}
