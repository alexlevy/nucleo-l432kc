use stm32l4::stm32l4x2::FLASH;

pub struct NewFlash;
pub struct FlashInitialized;

pub struct Flash<'a, STATE> {
    flash: &'a mut FLASH,
    _state: STATE,
}

pub enum Latency {
    ZeroWaitState,
    OneWaitState,
    TwoWaitState,
    ThreeWaitState,
    FourWaitState,
}

impl Latency {
    pub fn value(self) -> u8 {
        match self {
            Self::ZeroWaitState => 0b000,
            Self::OneWaitState => 0b001,
            Self::TwoWaitState => 0b010,
            Self::ThreeWaitState => 0b011,
            Self::FourWaitState => 0b100,
        }
    }
}

impl<'a> Flash<'a, NewFlash> {
    pub fn new(flash: &'a mut FLASH) -> Self {
        Flash {
            flash,
            _state: NewFlash,
        }
    }

    pub fn zero_wait_state(self) -> Flash<'a, FlashInitialized> {
        self.flash
            .acr
            .modify(|_, w| unsafe { w.latency().bits(Latency::ZeroWaitState.value()) });

        Flash {
            flash: self.flash,
            _state: FlashInitialized,
        }
    }

    pub fn one_wait_state(self) -> Flash<'a, FlashInitialized> {
        self.flash
            .acr
            .modify(|_, w| unsafe { w.latency().bits(Latency::OneWaitState.value()) });

        Flash {
            flash: self.flash,
            _state: FlashInitialized,
        }
    }

    pub fn two_wait_state(self) -> Flash<'a, FlashInitialized> {
        self.flash
            .acr
            .modify(|_, w| unsafe { w.latency().bits(Latency::TwoWaitState.value()) });

        Flash {
            flash: self.flash,
            _state: FlashInitialized,
        }
    }

    pub fn three_wait_state(self) -> Flash<'a, FlashInitialized> {
        self.flash
            .acr
            .modify(|_, w| unsafe { w.latency().bits(Latency::ThreeWaitState.value()) });

        Flash {
            flash: self.flash,
            _state: FlashInitialized,
        }
    }

    pub fn four_wait_state(self) -> Flash<'a, FlashInitialized> {
        self.flash
            .acr
            .modify(|_, w| unsafe { w.latency().bits(Latency::FourWaitState.value()) });

        Flash {
            flash: self.flash,
            _state: FlashInitialized,
        }
    }
}
