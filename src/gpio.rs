use core::marker::PhantomData;

// STATE
pub struct GPIODisabled;
pub struct GPIOEnabled;

// TYPE
pub struct UnknownMode;
pub struct Input<PT> {
    _pt: PhantomData<PT>,
}
pub struct Output<OT, PT> {
    _ot: PhantomData<OT>,
    _pt: PhantomData<PT>,
}
pub struct Floating;
pub struct PullUp;
pub struct PullDown;

pub struct PushPull;
pub struct OpenDrain;

#[macro_export]
macro_rules! gpio {
    ($($GPIOx:ident $([ $($NVICx:ident);+ ])? {$($PXi:expr $(=>$($EXTICRx:ident=$EXTICRval:expr,$IMRx:ident,$FTSRx:ident=$FTSRen:ident,$RTSRx:ident=$RTSRen:ident)? $(+$AFRx:ident=$AFx:ident)?)?);+})+) => {
        pub struct Peripheral<GPIO, STATE> {
            _state: PhantomData<STATE>,
            _gpio: PhantomData<GPIO>,
        }

        $(
            paste::paste! {
                use crate::gpio::*;

                impl Peripheral<stm32l4::stm32l4x2::[<GPIO $GPIOx>], GPIODisabled> {
                    pub fn enable(rcc: &mut RCC) -> Peripheral<stm32l4::stm32l4x2::[<GPIO $GPIOx>], GPIOEnabled> {
                        rcc.ahb2enr.modify(|_, w| w.[<gpio $GPIOx:lower en>]().set_bit());

                        rcc.ahb2rstr.modify(|_, w| w.[<gpio $GPIOx:lower rst>]().set_bit());
                        rcc.ahb2rstr.modify(|_, w| w.[<gpio $GPIOx:lower rst>]().clear_bit());

                        $(
                            use cortex_m::peripheral::NVIC;
                            use stm32l4::stm32l4x2::Interrupt;

                            rcc.apb2enr.modify(|_, w| w.syscfgen().set_bit());
                            $(
                                unsafe { NVIC::unmask(Interrupt::$NVICx) };
                            )+
                        )?

                        Peripheral {
                            _state: PhantomData,
                            _gpio: PhantomData,
                        }
                    }
                }

                impl Peripheral<stm32l4::stm32l4x2::[<GPIO $GPIOx>], GPIOEnabled> {
                    pub fn disable(&self, rcc: &mut RCC) -> Peripheral<stm32l4::stm32l4x2::[<GPIO $GPIOx>], GPIODisabled> {
                        rcc.ahb2enr.modify(|_, w| w.[<gpio $GPIOx:lower en>]().clear_bit());

                        Peripheral {
                            _state: PhantomData,
                            _gpio: PhantomData,
                        }
                    }
                }

                $(
                    pub struct [<P $GPIOx $PXi>]<TYPE> {
                        _type: PhantomData<TYPE>,
                    }

                    impl [<P $GPIOx $PXi>]<UnknownMode> {
                        pub fn new(_: &Peripheral<stm32l4::stm32l4x2::[<GPIO $GPIOx>], GPIOEnabled>) -> Self {
                            [<P $GPIOx $PXi>] {
                                _type: PhantomData,
                            }
                        }
                    }

                    impl<PT> [<P $GPIOx $PXi>]<Input<PT>> {
                        pub fn pull_up(&self, gpio: &mut stm32l4::stm32l4x2::[<GPIO $GPIOx>]) -> [<P $GPIOx $PXi>]<Input<PullUp>> {
                            gpio.pupdr.modify(|_, w| w.[<pupdr $PXi>]().pull_up());

                            [<P $GPIOx $PXi>] {
                                _type: PhantomData,
                            }
                        }

                        pub fn pull_down(&self, gpio: &mut stm32l4::stm32l4x2::[<GPIO $GPIOx>]) -> [<P $GPIOx $PXi>]<Input<PullDown>> {
                            gpio.pupdr.modify(|_, w| w.[<pupdr $PXi>]().pull_down());

                            [<P $GPIOx $PXi>] {
                                _type: PhantomData,
                            }
                        }

                        pub fn floating(&self, gpio: &mut stm32l4::stm32l4x2::[<GPIO $GPIOx>]) -> [<P $GPIOx $PXi>]<Input<Floating>> {
                            gpio.pupdr.modify(|_, w| w.[<pupdr $PXi>]().floating());

                            [<P $GPIOx $PXi>] {
                                _type: PhantomData,
                            }
                        }

                        pub fn read(&self, gpio: &mut stm32l4::stm32l4x2::[<GPIO $GPIOx>]) -> stm32l4::BitReaderRaw<stm32l4::stm32l4x2::[<gpio $GPIOx:lower>]::idr::IDR15_A> {
                            gpio.idr.read().[<idr $PXi>]()
                        }
                    }

                    impl<TYPE> [<P $GPIOx $PXi>]<TYPE> {
                        $(
                            $(
                                pub fn configure_interrupt(&self, syscfg: &mut SYSCFG, exti: &mut EXTI) -> Self {
                                syscfg
                                    .$EXTICRx
                                    .modify(|_, w| unsafe { w.[<exti $PXi>]().bits($EXTICRval) });
                                exti.$RTSRx.modify(|_, w| w.[<tr $PXi>]().$RTSRen());
                                exti.$FTSRx.modify(|_, w| w.[<tr $PXi>]().$FTSRen());

                                [<P $GPIOx $PXi>] { _type: PhantomData }
                                }

                                pub fn disable_interrupt(&self, exti: &mut EXTI) -> Self {
                                    exti.$IMRx.modify(|_, w| w.[<mr $PXi>]().masked());

                                    [<P $GPIOx $PXi>] { _type: PhantomData }
                                }

                                pub fn enable_interrupt(&self, exti: &mut EXTI) -> Self {
                                    exti.$IMRx.modify(|_, w| w.[<mr $PXi>]().unmasked());

                                    [<P $GPIOx $PXi>] { _type: PhantomData }
                                }
                            )?
                        )?

                        $(
                            $(
                                pub fn into_alternate(&self, gpio: &mut stm32l4::stm32l4x2::[<GPIO $GPIOx>]) -> Self {
                                    gpio.moder.modify(|_ ,w| w.[<moder $PXi>]().alternate());
                                    gpio.$AFRx.modify(|_, w| w.[<$AFRx $PXi>]().$AFx());

                                    [<P $GPIOx $PXi>] { _type: PhantomData }
                                }
                            )?
                        )?

                        pub fn into_input(&self, gpio: &mut stm32l4::stm32l4x2::[<GPIO $GPIOx>]) -> [<P $GPIOx $PXi>]<Input<Floating>> {
                            gpio.moder.modify(|_, w| w.[<moder $PXi>]().input());
                            gpio.pupdr.modify(|_, w| w.[<pupdr $PXi>]().floating());

                            [<P $GPIOx $PXi>] {
                                _type: PhantomData,
                            }
                        }

                        pub fn into_output(&self, gpio: &mut stm32l4::stm32l4x2::[<GPIO $GPIOx>]) -> [<P $GPIOx $PXi>]<Output<PushPull, Floating>> {
                            gpio.moder.modify(|_, w| w.[<moder $PXi>]().output());
                            gpio.pupdr.modify(|_, w| w.[<pupdr $PXi>]().floating());

                            [<P $GPIOx $PXi>] {
                                _type: PhantomData,
                            }
                        }
                    }

                    impl<OT, PT> [<P $GPIOx $PXi>]<Output<OT, PT>> {
                        pub fn pull_up(&self, gpio: &mut stm32l4::stm32l4x2::[<GPIO $GPIOx>]) -> [<P $GPIOx $PXi>]<Output<OT,PullUp>> {
                            gpio.pupdr.modify(|_, w| w.[<pupdr $PXi>]().pull_up());

                            [<P $GPIOx $PXi>] {
                                _type: PhantomData,
                            }
                        }

                        pub fn pull_down(&self, gpio: &mut stm32l4::stm32l4x2::[<GPIO $GPIOx>]) -> [<P $GPIOx $PXi>]<Output<OT,PullDown>> {
                            gpio.pupdr.modify(|_, w| w.[<pupdr $PXi>]().pull_down());

                            [<P $GPIOx $PXi>] {
                                _type: PhantomData,
                            }
                        }

                        pub fn floating(&self, gpio: &mut stm32l4::stm32l4x2::[<GPIO $GPIOx>]) -> [<P $GPIOx $PXi>]<Output<OT,Floating>> {
                            gpio.pupdr.modify(|_, w| w.[<pupdr $PXi>]().floating());

                            [<P $GPIOx $PXi>] {
                                _type: PhantomData,
                            }
                        }

                        pub fn push_pull(&self, gpio: &mut stm32l4::stm32l4x2::[<GPIO $GPIOx>]) -> [<P $GPIOx $PXi>]<Output<PushPull, PT>> {
                            gpio.otyper.modify(|_, w| w.[<ot $PXi>]().push_pull());

                            [<P $GPIOx $PXi>] {
                                _type: PhantomData,
                            }
                        }

                        pub fn open_drain(&self, gpio: &mut stm32l4::stm32l4x2::[<GPIO $GPIOx>]) -> [<P $GPIOx $PXi>]<Output<OpenDrain, PT>> {
                            gpio.otyper.modify(|_, w| w.[<ot $PXi>]().open_drain());

                            [<P $GPIOx $PXi>] {
                                _type: PhantomData,
                            }
                        }

                        pub fn set_high(&self, gpio: &mut stm32l4::stm32l4x2::[<GPIO $GPIOx>]) -> Self {
                            gpio.odr.modify(|_, w| w.[<odr $PXi>]().set_bit());

                            [<P $GPIOx $PXi>] {
                                _type: PhantomData,
                            }
                        }

                        pub fn set_low(&self, gpio: &mut stm32l4::stm32l4x2::[<GPIO $GPIOx>]) -> Self {
                            gpio.odr.modify(|_, w| w.[<odr $PXi>]().clear_bit());

                            [<P $GPIOx $PXi>] {
                                _type: PhantomData,
                            }
                        }
                    }
                )+
            }
        )+
    };
}
