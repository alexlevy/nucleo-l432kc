pub struct I2CActivated;
pub struct I2CDeactivated;
pub struct I2CConfiguring;
pub struct I2CEnabled;
pub struct I2CDisabled;

#[macro_export]
macro_rules! i2c {
    ($($I2Cx:expr => presc=$PRESC:expr,sdadel=$SDADEL:expr,scldel=$SCLDEL:expr,scll=$SCLL:expr,sclh=$SCLH:expr);+) => {
            pub struct I2C<_I2C, STATE> {
                _state: PhantomData<STATE>,
                _i2c: PhantomData<_I2C>,
            }

            use crate::i2c::*;

            paste::paste!{
                $(
                    impl I2C<stm32l4::stm32l4x2::[<I2C $I2Cx>], I2CDeactivated> {
                        pub fn activate(rcc: &mut RCC) -> I2C<stm32l4::stm32l4x2::[<I2C $I2Cx>], I2CActivated> {
                            rcc.apb1enr1.modify(|_, w| w.[<i2c $I2Cx en>]().set_bit());
                            rcc.apb1rstr1.modify(|_, w| w.[<i2c $I2Cx rst>]().set_bit());
                            rcc.apb1rstr1.modify(|_, w| w.[<i2c $I2Cx rst>]().clear_bit());

                            I2C { _state: PhantomData, _i2c: PhantomData }
                        }
                    }

                    impl I2C<stm32l4::stm32l4x2::[<I2C $I2Cx>], I2CActivated> {
                        pub fn deactivate(rcc: &mut RCC) -> I2C<stm32l4::stm32l4x2::[<I2C $I2Cx>], I2CDeactivated> {
                            rcc.apb1enr1.modify(|_, w| w.[<i2c $I2Cx en>]().clear_bit());

                            I2C { _state: PhantomData, _i2c: PhantomData }
                        }

                        pub fn configure(&self, i2c: &mut stm32l4::stm32l4x2::[<I2C $I2Cx>]) -> I2C<stm32l4::stm32l4x2::[<I2C $I2Cx>], I2CConfiguring> {
                            i2c.cr1.modify(|_, w| w.pe().clear_bit());

                            I2C { _state: PhantomData, _i2c: PhantomData }
                        }
                    }

                    impl I2C<stm32l4::stm32l4x2::[<I2C $I2Cx>], I2CConfiguring> {
                        pub fn enable_analog_filter(&self, i2c: &mut stm32l4::stm32l4x2::[<I2C $I2Cx>]) -> Self {
                            i2c.cr1.modify(|_, w| w.anfoff().clear_bit());

                            I2C { _state: PhantomData, _i2c: PhantomData }
                        }

                        pub fn disable_analog_filter(&self, i2c: &mut stm32l4::stm32l4x2::[<I2C $I2Cx>]) -> Self {
                            i2c.cr1.modify(|_, w| w.anfoff().set_bit());

                            I2C { _state: PhantomData, _i2c: PhantomData }
                        }

                        pub fn configure_timings(&self, i2c: &mut stm32l4::stm32l4x2::[<I2C $I2Cx>]) -> Self {
                            i2c.timingr.modify(|_, w| {
                                w.sdadel()
                                .bits($SDADEL)
                                .scldel()
                                .bits($SCLDEL)
                                .scll()
                                .bits($SCLL)
                                .sclh()
                                .bits($SCLH)
                                .presc()
                                .bits($PRESC)
                            });

                            I2C { _state: PhantomData, _i2c: PhantomData }
                        }

                        pub fn enable(&self, i2c: &mut stm32l4::stm32l4x2::[<I2C $I2Cx>]) -> I2C<stm32l4::stm32l4x2::[<I2C $I2Cx>], I2CEnabled> {
                            i2c.cr1.modify(|_, w| w.pe().set_bit());

                            I2C { _state: PhantomData, _i2c: PhantomData }
                        }
                    }

                    impl I2C<stm32l4::stm32l4x2::[<I2C $I2Cx>], I2CEnabled> {
                        pub fn write(&self, i2c: &mut stm32l4::stm32l4x2::[<I2C $I2Cx>], slave_addr: u16, bytes: &[u8]) {
                            i2c.cr2.modify(|_, w| {
                                w.add10()
                                    .bit7()
                                    .sadd()
                                    .bits(slave_addr << 1)
                                    .rd_wrn()
                                    .write()
                                    .nbytes()
                                    .bits(bytes.len() as u8)
                                    .autoend()
                                    .software()
                                    .start()
                                    .start()
                            });

                            for byte in bytes {
                                while i2c.isr.read().txis().is_not_empty() {}
                                i2c.txdr.write(|w| w.txdata().bits(*byte));
                            }

                            while i2c.isr.read().tc().is_not_complete() {}

                            i2c.cr2.modify(|_, w| w.stop().set_bit());
                        }

                        pub fn read(&self, i2c: &mut stm32l4::stm32l4x2::[<I2C $I2Cx>], slave_addr: u16, buffer: &mut [u8]) {
                            i2c.cr2.modify(|_, w| {
                                w.add10()
                                    .bit7()
                                    .sadd()
                                    .bits(slave_addr << 1)
                                    .rd_wrn()
                                    .read()
                                    .nbytes()
                                    .bits(buffer.len() as u8)
                                    .autoend()
                                    .automatic()
                                    .start()
                                    .start()
                            });

                            for byte in buffer {
                                while i2c.isr.read().rxne().is_empty() {}
                                *byte = i2c.rxdr.read().rxdata().bits();
                            }
                        }

                        pub fn disable(&self, i2c: &mut stm32l4::stm32l4x2::[<I2C $I2Cx>]) -> I2C<stm32l4::stm32l4x2::[<I2C $I2Cx>], I2CDisabled> {
                            i2c.cr1.modify(|_, w| w.pe().clear_bit());

                            I2C { _state: PhantomData, _i2c: PhantomData }
                        }
                    }

                    impl I2C<stm32l4::stm32l4x2::[<I2C $I2Cx>], I2CDisabled> {
                        pub fn enable(&self, i2c: &mut stm32l4::stm32l4x2::[<I2C $I2Cx>]) -> I2C<stm32l4::stm32l4x2::[<I2C $I2Cx>], I2CEnabled> {
                            i2c.cr1.modify(|_, w| w.pe().set_bit());

                            I2C { _state: PhantomData, _i2c: PhantomData }
                        }
                    }
                )+
            }
    };
}
