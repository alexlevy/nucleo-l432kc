#[macro_export]
macro_rules! lcd_1602 {
    ($I2Cx:ident, $GPIOx:ident { $SCL:expr ; $SDA:expr }) => {
        use crate::i2c::*;
        use defmt::assert;

        paste::paste! {
            pub struct LCD<STATE> {
                _state: PhantomData<STATE>,
                address: u16,
            }

            pub struct LCDInitialized;
            pub struct LCDUninitialized;

            impl LCD<LCDUninitialized> {
                pub fn init(
                    rcc: &mut stm32l4::stm32l4x2::RCC,
                    gpio: &mut stm32l4::stm32l4x2::[<GPIO $GPIOx>],
                    address: u16,
                ) -> LCD<LCDInitialized> {
                    let periph = <Peripheral<stm32l4::stm32l4x2::[<GPIO $GPIOx>], _>>::enable(rcc);

                    [<P $GPIOx $SCL>]::new(&periph)
                        .into_output(gpio)
                        .open_drain(gpio)
                        .pull_up(gpio)
                        .into_alternate(gpio);

                    [<P $GPIOx $SDA>]::new(&periph)
                        .into_output(gpio)
                        .open_drain(gpio)
                        .pull_up(gpio)
                        .into_alternate(gpio);

                    LCD {
                        _state: PhantomData,
                        address,
                    }
                }
            }

            impl LCD<LCDInitialized> {
                pub fn clear_screen(
                    &self,
                    i2c: &I2C<stm32l4::stm32l4x2::$I2Cx, I2CEnabled>,
                    [<$I2Cx:lower>]: &mut stm32l4::stm32l4x2::$I2Cx
                ) -> &Self {
                    self.send_command(i2c, [<$I2Cx:lower>], &[0x01]);
                    self
                }

                fn print_char(
                    &self,
                    c: u8,
                    i2c: &I2C<stm32l4::stm32l4x2::$I2Cx, I2CEnabled>,
                    [<$I2Cx:lower>]: &mut stm32l4::stm32l4x2::$I2Cx
                ) {
                    assert!(c <= 0x7e && c >= 0x20);
                    self.send_command(i2c, [<$I2Cx:lower>], &[0x00, c]);
                }

                fn while_busy(
                    &self,
                    i2c: &I2C<stm32l4::stm32l4x2::$I2Cx, I2CEnabled>,
                    [<$I2Cx:lower>]: &mut stm32l4::stm32l4x2::$I2Cx
                ) {
                    let mut status: [u8; 1] = [0xff];
                    while status[0] & 0x80 != 0x00 {
                        i2c.read([<$I2Cx:lower>], self.address, &mut status);
                    }
                }

                pub fn print_string(
                    &self,
                    i2c: &I2C<stm32l4::stm32l4x2::$I2Cx, I2CEnabled>,
                    [<$I2Cx:lower>]: &mut stm32l4::stm32l4x2::$I2Cx,
                    s: &str
                ) -> &Self {
                    for c in s.bytes() {
                        self.print_char(c, i2c, [<$I2Cx:lower>]);
                    }
                    self
                }

                pub fn print_slice(
                    &self,
                    i2c: &I2C<stm32l4::stm32l4x2::$I2Cx, I2CEnabled>,
                    [<$I2Cx:lower>]: &mut stm32l4::stm32l4x2::$I2Cx,
                    s: &[u8]
                ) -> &Self {
                    for c in s {
                        self.print_char(*c, i2c, [<$I2Cx:lower>]);
                    }
                    self
                }

                pub fn set_cursor_position(
                    &self,
                    i2c: &I2C<stm32l4::stm32l4x2::$I2Cx, I2CEnabled>,
                    [<$I2Cx:lower>]: &mut stm32l4::stm32l4x2::$I2Cx,
                    row: u8,
                    column: u8,
                ) -> &Self {
                    assert!(row <= 0x03 && column <= 0x4f);
                    self.send_command(i2c, [<$I2Cx:lower>], &[0x02, row, column]);
                    self
                }

                pub fn define_custom_character(
                    &self,
                    i2c: &I2C<stm32l4::stm32l4x2::$I2Cx, I2CEnabled>,
                    [<$I2Cx:lower>]: &mut stm32l4::stm32l4x2::$I2Cx,
                    character: u8,
                    parameters: &[u8]
                ) -> &Self {
                    assert!(parameters.len() == 8);
                    assert!(character <= 0x07);

                    let mut full_command: heapless::Vec<u8, 10> = heapless::Vec::new();

                    full_command.push(0x08).ok().unwrap();
                    full_command.push(character).ok().unwrap();

                    for p in parameters {
                        full_command.push(*p).ok().unwrap();
                    }

                    self.send_command(i2c, [<$I2Cx:lower>], full_command.as_slice());

                    self
                }

                pub fn print_custom_character(
                    &self,
                    i2c: &I2C<stm32l4::stm32l4x2::$I2Cx, I2CEnabled>,
                    [<$I2Cx:lower>]: &mut stm32l4::stm32l4x2::$I2Cx,
                    character: u8
                ) -> &Self {
                    assert!(character <= 0x07);
                    self.send_command(i2c, [<$I2Cx:lower>], &[0x07, character]);
                    self
                }

                fn send_command(
                    &self,
                    i2c: &I2C<stm32l4::stm32l4x2::$I2Cx, I2CEnabled>,
                    [<$I2Cx:lower>]: &mut stm32l4::stm32l4x2::$I2Cx,
                    bytes: &[u8],
                ) {
                    self.while_busy(i2c, [<$I2Cx:lower>]);
                    i2c.write([<$I2Cx:lower>], self.address, bytes);
                }
            }
        }
    };
}
