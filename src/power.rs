use stm32l4::stm32l4x2::{PWR, RCC};

pub struct PowerEnabled;
pub struct PowerDisabled;

pub struct Power<'a, STATE> {
    rcc: &'a mut RCC,
    pwr: &'a mut PWR,
    _state: STATE,
}

enum VOS {
    Range1,
    Range2,
}

impl VOS {
    fn value(self) -> u8 {
        match self {
            Self::Range1 => 0b01,
            Self::Range2 => 0b10,
        }
    }
}

impl<'a, STATE> Power<'a, STATE> {
    pub fn free(self) -> &'a mut RCC {
        self.rcc
    }
}

impl<'a> Power<'a, PowerDisabled> {
    pub fn new(rcc: &'a mut RCC, pwr: &'a mut PWR) -> Self {
        Power {
            rcc,
            pwr,
            _state: PowerDisabled,
        }
    }
    pub fn enable(self) -> Power<'a, PowerEnabled> {
        self.rcc.apb1enr1.modify(|_, w| w.pwren().set_bit());

        Power {
            rcc: self.rcc,
            pwr: self.pwr,
            _state: PowerEnabled,
        }
    }
}

impl<'a> Power<'a, PowerEnabled> {
    pub fn disable(self) -> Power<'a, PowerDisabled> {
        self.rcc.apb1enr1.modify(|_, w| w.pwren().clear_bit());

        Power {
            rcc: self.rcc,
            pwr: self.pwr,
            _state: PowerDisabled,
        }
    }

    pub fn range1(self) -> Self {
        self.pwr
            .cr1
            .modify(|_, w| unsafe { w.vos().bits(VOS::Range1.value()) });
        self
    }

    pub fn range2(self) -> Self {
        self.pwr
            .cr1
            .modify(|_, w| unsafe { w.vos().bits(VOS::Range2.value()) });
        self
    }
}
