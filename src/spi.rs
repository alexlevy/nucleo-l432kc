pub struct SPIDeactivated;
pub struct SPIActivated;
pub struct SPIConfigured;
pub struct SPIEnabled;
pub struct SPIDisabled;

pub enum DataSize {
    _4Bits = 0b011,
    _5Bits = 0b0100,
    _6Bits = 0b0101,
    _7Bits = 0b0110,
    _8Bits = 0b0111,
    _9Bits = 0b1000,
    _10Bits = 0b1001,
    _11Bits = 0b1010,
    _12Bits = 0b1011,
    _13Bits = 0b1100,
    _14Bits = 0b1101,
    _15Bits = 0b1110,
    _16Bits = 0b1111,
}

pub enum BaudRate {
    Div2 = 0b000,
    Div4 = 0b001,
    Div8 = 0b010,
    Div16 = 0b011,
    Div32 = 0b100,
    Div64 = 0b101,
    Div128 = 0b110,
    Div256 = 0b111,
}

#[macro_export]
macro_rules! spi {
    ($($SPIx:expr => $([ txeie=$TXEIE:expr,rxneie=$RXNEIE:expr,errie=$ERRIE:expr ])? {
       $RST:ident,
       $EN:ident,
       $(bidimode=$BIDIMODE:expr,bidioe=$BIDIOE:expr,)?
       $(crcen=$CRCEN:expr,crcl=$CRCL:expr,)?
       $(rxonly=$RXONLY:expr,)?
       ssm=$SSM:expr,
       $(ssi=$SSI:expr,)?
       lsbfirst=$LSBFIRST:expr,
       br=$BR:expr,
       mstr=$MSTR:expr,
       cpol=$CPOL:expr,
       cpha=$CPHA:expr,
       $(frxth=$FRXTH:expr,)?
       $(nssp=$NSSP:expr,)?
       $(ssoe=$SSOE:expr,)?
       ds=$DS:tt
    })+
   ) => {
        pub struct SPI<_SPI, STATE> {
            _state: PhantomData<STATE>,
            _spi: PhantomData<_SPI>,
        }

        use crate::spi::*;

        paste::paste! {
            $(
                impl SPI<stm32l4::stm32l4x2::[<SPI $SPIx>], SPIDeactivated> {
                    pub fn activate(rcc: &mut RCC) -> SPI<stm32l4::stm32l4x2::[<SPI $SPIx>], SPIActivated> {
                        rcc.$EN.modify(|_, w| w.[<spi $SPIx en>]().set_bit());
                        rcc.$RST.modify(|_, w| w.[<spi $SPIx rst>]().set_bit());
                        rcc.$RST.modify(|_, w| w.[<spi $SPIx rst>]().clear_bit());

                        // TODO: do this once parsing txeie etc.
                        // $(
                        //     use cortex_m::peripheral::NVIC;
                        //     use stm32l4::stm32l4x2::Interrupt;

                        //     unsafe { NVIC::unmask(Interrupt::$SPIx) };
                        // )?


                        SPI { _state: PhantomData, _spi: PhantomData }
                    }
                }

                impl SPI<stm32l4::stm32l4x2::[<SPI $SPIx>], SPIActivated> {
                    pub fn configure(&self, spi: &mut stm32l4::stm32l4x2::[<SPI $SPIx>]) -> SPI<stm32l4::stm32l4x2::[<SPI $SPIx>], SPIConfigured> {
                        spi.cr1.modify(|_, w| {
                            let w = unsafe { w.br().bits(BaudRate::$BR as u8) }     // set baud rate
                                              .cpol()               // set clock polarity
                                              .bit($CPOL != 0)
                                              .cpha()               // set clock phase
                                              .bit($CPHA != 0);
                            $(
                                let w = w.rxonly()                  // Set rx only
                                         .bit($RXONLY != 0);

                            )?

                            $(
                                let w = w.bidimode()                // Select simplex of half-duplex
                                         .bit($BIDIMODE != 0)
                                         .bidioe()                  // Set output enabled
                                         .bit($BIDIOE != 0);
                            )?

                            let w = w.lsbfirst()                    // Set frame format
                                     .bit($LSBFIRST != 0);

                            $(
                                let w = w.crcl()                    // Set CRC length
                                         .bit($CRCL != 0)
                                         .crcen()                   // Enable/disable CRC
                                         .bit($CRCEN != 0);
                            )?

                            $(
                                let w = w.ssm()                     // Set software slave management
                                         .bit($SSM != 0)
                                         .ssi()                     // Set internal slave select
                                         .bit($SSI != 0);
                            )?

                            w.mstr()                                // Set master/slave mode
                             .bit($MSTR != 0)
                        });

                        spi.cr2.modify(|_, w| {
                            let w = unsafe { w.ds().bits(DataSize::[<_ $DS>] as u8) };      // Set data length

                            $(
                                let w = w.ssoe().bit($SSOE != 0);
                            )?

                            $(
                                let w = w.nssp().bit($NSSP != 0);
                            )?

                            $(
                                let w = w.frxth().bit($FRXTH != 0);
                            )?

                            w
                        });

                        SPI { _state: PhantomData, _spi: PhantomData }
                    }

                    pub fn set_crc_polynomial(&self, spi: &mut stm32l4::stm32l4x2::[<SPI $SPIx>], crcpoly: u16) -> Self {
                        spi.crcpr.write(|w| unsafe { w.crcpoly().bits(crcpoly) });

                        SPI { _state: PhantomData, _spi: PhantomData }
                    }
                }

                impl SPI<stm32l4::stm32l4x2::[<SPI $SPIx>], SPIConfigured> {
                    pub fn enable(&self, spi: &mut stm32l4::stm32l4x2::[<SPI $SPIx>]) -> SPI<stm32l4::stm32l4x2::[<SPI $SPIx>], SPIEnabled> {
                        spi.cr1.modify(|_, w| w.spe().set_bit());

                        SPI { _state: PhantomData, _spi: PhantomData }
                    }

                    pub fn disable(&self, spi: &mut stm32l4::stm32l4x2::[<SPI $SPIx>]) -> SPI<stm32l4::stm32l4x2::[<SPI $SPIx>], SPIDisabled> {
                        spi.cr1.modify(|_, w| w.spe().clear_bit());

                        SPI { _state: PhantomData, _spi: PhantomData }
                    }
                }

                impl SPI<stm32l4::stm32l4x2::[<SPI $SPIx>], SPIEnabled> {
                    pub fn disable(&self, spi: &mut stm32l4::stm32l4x2::[<SPI $SPIx>]) -> SPI<stm32l4::stm32l4x2::[<SPI $SPIx>], SPIDisabled> {
                        spi.cr1.modify(|_, w| w.spe().clear_bit());

                        SPI { _state: PhantomData, _spi: PhantomData }
                    }
                }

                impl SPI<stm32l4::stm32l4x2::[<SPI $SPIx>], SPIDisabled> {
                    pub fn enable(&self, spi: &mut stm32l4::stm32l4x2::[<SPI $SPIx>]) -> SPI<stm32l4::stm32l4x2::[<SPI $SPIx>], SPIEnabled> {
                        spi.cr1.modify(|_, w| w.spe().set_bit());

                        SPI { _state: PhantomData, _spi: PhantomData }
                    }

                    pub fn deactivate(&self, rcc: &mut stm32l4::stm32l4x2::RCC) -> SPI<stm32l4::stm32l4x2::[<SPI $SPIx>], SPIDeactivated> {
                        rcc.$EN.modify(|_, w| w.[<spi $SPIx en>]().clear_bit());

                        SPI { _state: PhantomData, _spi: PhantomData }
                    }
                }
            )+
        }
    };
}
