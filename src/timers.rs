pub struct TimerConfigured;
pub struct NewTimer;
pub struct TimerStarted;
pub struct TimerStopped;

#[macro_export]
macro_rules! timers {
    ($($TIMx:ident $([ $NVICx:ident ])?);+) => {
        pub mod basic_timers {
            use core::marker::PhantomData;
            use cortex_m::peripheral::NVIC;
            use stm32l4::stm32l4x2::{RCC, tim6};
            use core::ops::Deref;

            pub struct Timer<TIM, STATE> {
                state: PhantomData<STATE>,
                _tim: PhantomData<TIM>,
            }

            pub trait Startable<TIM>
            where
                TIM: Deref<Target = tim6::RegisterBlock>
            {
                fn start(&self, tim: &mut TIM) -> Timer<TIM, TimerStarted> {
                    tim.cr1.modify(|_, w| w.cen().set_bit());
                    Timer { state: PhantomData, _tim: PhantomData }
                }
            }

            $(
                paste::paste! {
                    $(
                        use stm32l4::stm32l4x2::interrupt::$NVICx;
                    )?
                    use stm32l4::stm32l4x2::$TIMx;
                    use crate::timers::*;

                    impl Startable<$TIMx> for Timer<$TIMx, TimerConfigured> {}

                    impl Startable<$TIMx> for Timer<$TIMx, TimerStopped> {}

                    impl Timer<$TIMx, NewTimer> {
                        pub fn new(tim: &mut $TIMx, rcc: &mut RCC) -> Self {
                            // Enable TIMx clock
                            rcc.apb1enr1.modify(|_, w| w.[<$TIMx:lower en>]().set_bit());

                            // Reset TIMx peripheral
                            rcc.apb1rstr1.modify(|_, w| w.[<$TIMx:lower rst>]().set_bit());
                            rcc.apb1rstr1.modify(|_, w| w.[<$TIMx:lower rst>]().clear_bit());

                            // Enable TIMx update events
                            tim.cr1.modify(|_, w| w.udis().clear_bit());

                            Timer { state: PhantomData, _tim: PhantomData }
                        }
                    }

                    impl Timer<$TIMx, TimerStarted> {
                        pub fn wait_for_overflow(&self, tim: &mut $TIMx) -> Timer<$TIMx, TimerStopped> {
                            while tim.sr.read().uif().bit_is_clear() {}
                            tim.sr.modify(|_, w| w.uif().clear_bit());

                            Timer { state: PhantomData, _tim: PhantomData }
                        }
                    }

                    impl<STATE> Timer<$TIMx, STATE> {
                        pub fn one_shot(&self, tim: &mut $TIMx) -> Timer<$TIMx, TimerConfigured> {
                            tim.cr1.modify(|_, w| w.opm().set_bit());
                            Timer { state: PhantomData, _tim: PhantomData }
                        }

                        pub fn continuous(&self, tim: &mut $TIMx) -> Timer<$TIMx, TimerConfigured> {
                            tim.cr1.modify(|_, w| w.opm().clear_bit());

                            Timer { state: PhantomData, _tim: PhantomData }
                        }

                        pub fn clear_update_interrupt_flag(&self, tim: &mut $TIMx) -> Self {
                            tim.sr.modify(|_, w| w.uif().clear_bit());
                            Timer { state: PhantomData, _tim: PhantomData }
                        }

                        pub fn trigger_update(&self, tim: &mut $TIMx) -> Self {
                            tim.egr.write(|w| w.ug().set_bit());
                            Timer { state: PhantomData, _tim: PhantomData }
                        }

                        pub fn stop(&self, tim: &mut $TIMx) -> Timer<$TIMx, TimerStopped> {
                            tim.cr1.modify(|_, w| w.cen().clear_bit());

                            Timer { state: PhantomData, _tim: PhantomData }
                        }

                        $(
                            pub fn enable_interrupts(&self, tim: &mut $TIMx) -> Self {
                                tim.dier.modify(|_, w| w.uie().set_bit());
                                unsafe { NVIC::unmask($NVICx) };
                                Timer { state: PhantomData, _tim: PhantomData }
                            }

                            pub fn disable_interrupts(&self, tim: &mut $TIMx) -> Self {
                                tim.dier.modify(|_, w| w.uie().clear_bit());
                                NVIC::mask($NVICx);
                                Timer { state: PhantomData, _tim: PhantomData }
                            }
                        )?

                        pub fn enable_updates(&self, tim: &mut $TIMx) -> Self {
                            tim.cr1.modify(|_, w| w.udis().clear_bit());
                            Timer { state: PhantomData, _tim: PhantomData }
                        }

                        pub fn disable_updates(&self, tim: &mut $TIMx) -> Self {
                            tim.cr1.modify(|_, w| w.udis().set_bit());
                            Timer { state: PhantomData, _tim: PhantomData }
                        }

                        pub fn set_update_request_source(&self, tim: &mut $TIMx) -> Self {
                            tim.cr1.modify(|_, w| w.urs().set_bit());
                            Timer { state: PhantomData, _tim: PhantomData }
                        }

                        pub fn unset_update_request_source(&self, tim: &mut $TIMx) -> Self {
                            tim.cr1.modify(|_, w| w.urs().clear_bit());
                            Timer { state: PhantomData, _tim: PhantomData }
                        }

                        /// Load ARR and PSC registers
                        ///
                        /// An update event is manually trigger so that the prescaler value is loaded.
                        ///
                        /// *Beware !* This triggers an interrupt
                        pub fn load(&self, tim: &mut $TIMx, ticks: u16, prescaler: Option<u16>) -> Self {
                            let cur_prescaler = tim.psc.read().psc().bits();

                            // if given:
                            // * 0 ==> 0
                            // * 1 ==> 0
                            // * 2 ==> 1
                            // * 10 ==> 9
                            let p = prescaler
                                .map(|x| if x >= 2 { x - 1 } else { 0 })
                                .unwrap_or(cur_prescaler);

                            if p != cur_prescaler {
                                tim.psc.write(|w| w.psc().bits(p));
                                tim.egr.write(|w| w.ug().set_bit());
                                tim.sr.modify(|_, w| w.uif().clear_bit());
                            }

                            tim.arr.write(|w| w.arr().bits(ticks as u16));

                            Timer { state: PhantomData, _tim: PhantomData }
                        }
                    }
                }
            )+
        }
    };
}
